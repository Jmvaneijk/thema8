# README #

To reproduce all the tests that have been described on this repo the following is needed:
    - [Rstudio](https://rstudio.com/)
    - [LaTeX for Rstudio](https://www.latex-project.org/)
    - [The Desolve Package](https://cran.r-project.org/web/packages/deSolve/index.html)

After the mentioned software has been installed the scripts can be downloaded and used.

## [Introduction](https://bitbucket.org/Jmvaneijk/thema8/src/master/Introduction/) ##

At the start of the project a few small tests and simulations were done described here

## [mRNA_Dynamic](https://bitbucket.org/Jmvaneijk/thema8/src/master/mRNA_Dynamic/) ##

The first real simulation that was made had to do with the synthesis and degradation of mRNA.
This folder contains the images, .pdf and .rdm files for the mRNA_Dynamic exercise.

## [Glucocortico](https://bitbucket.org/Jmvaneijk/thema8/src/master/Glucocortico/)  ##

The Glucocortico simulation is spread over multiple excercises, each for a different week.
So each weeks folder contains the images, .pdf and .rdm files for that week. 
The weeks start at week 2 since week 1 was used for the mRNA Dynamic

### [Week 2](https://bitbucket.org/Jmvaneijk/thema8/src/master/Glucocortico/Week2/)

For Week 2 the model for the glucocortico mechanism was programmed and explained.
The files to redo the code can be found in the folder

### [Week 3&4](https://bitbucket.org/Jmvaneijk/thema8/src/master/Glucocortico/Week3&4/) ###

For week 3 the model was tested against an experiment and different scenarios were tested.
The files to redo the code can be found in the folder

The fourth week is also in this folder. For this week the PDF file was reviewed by another group.
A few additions were made but noting big enough for a new folder.

## [Methane Emission](https://bitbucket.org/Jmvaneijk/thema8/src/master/Methane_Emission/)

As a final excercise a publication using deSolve was reproduced. [the publication](https://www.sciencedirect.com/science/article/pii/S2351989418302075)
that was chosen describes a model of goat metabolism. This model can be used to calculate milk and methane production of goats among other things.
the R-code for the model and the figures can all be found in the folder.

To use the beforementioned R-code a few packages need to be installed first. For starters [the deSolve package](https://cran.r-project.org/web/packages/deSolve/index.html)
needs to be installed. The other packages that are required are the [reshape2](https://cran.r-project.org/web/packages/reshape2/index.html) and
[ggplot2](https://cran.r-project.org/web/packages/ggplot2/index.html) packages. Once all of these have correctly been installed, the code can be used.